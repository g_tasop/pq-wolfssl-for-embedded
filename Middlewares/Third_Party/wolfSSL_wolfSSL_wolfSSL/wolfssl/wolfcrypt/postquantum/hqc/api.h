#ifndef PQCLEAN_HQCRMRS128_CLEAN_API_H
#define PQCLEAN_HQCRMRS128_CLEAN_API_H
/**
 * @file api.h
 * @brief NIST KEM API used by the HQC_KEM IND-CCA2 scheme
 */

#define PQCLEAN_HQCRMRS128_CLEAN_CRYPTO_ALGNAME                      "HQC-RMRS-128"

#define PQCLEAN_HQCRMRS128_CLEAN_CRYPTO_SECRETKEYBYTES               2289
#define PQCLEAN_HQCRMRS128_CLEAN_CRYPTO_PUBLICKEYBYTES               2249
#define PQCLEAN_HQCRMRS128_CLEAN_CRYPTO_BYTES                        64
#define PQCLEAN_HQCRMRS128_CLEAN_CRYPTO_CIPHERTEXTBYTES              4481

/* -------------- DIKA MOU ------------------ */

#undef CRYPTO_PUBLICKEYBYTES
#undef CRYPTO_SECRETKEYBYTES
#undef CRYPTO_BYTES
#undef CRYPTO_ALGNAME

#define CRYPTO_SECRETKEYBYTES  PQCLEAN_HQCRMRS128_CLEAN_CRYPTO_SECRETKEYBYTES
#define CRYPTO_PUBLICKEYBYTES  PQCLEAN_HQCRMRS128_CLEAN_CRYPTO_PUBLICKEYBYTES
#define CRYPTO_BYTES           PQCLEAN_HQCRMRS128_CLEAN_CRYPTO_BYTES
#define CRYPTO_CIPHERTEXTBYTES PQCLEAN_HQCRMRS128_CLEAN_CRYPTO_CIPHERTEXTBYTES
#define CRYPTO_ALGNAME   	   PQCLEAN_HQCRMRS128_CLEAN_CRYPTO_ALGNAME


/* -------------- DIKA MOU MEXRI EDW ------------------ */

// As a technicality, the public key is appended to the secret key in order to respect the NIST API.
// Without this constraint, PQCLEAN_HQCRMRS128_CLEAN_CRYPTO_SECRETKEYBYTES would be defined as 32

int hqc_crypto_kem_keypair(unsigned char *pk, unsigned char *sk);

int hqc_crypto_kem_enc(unsigned char *ct, unsigned char *ss, const unsigned char *pk);

int hqc_crypto_kem_dec(unsigned char *ss, const unsigned char *ct, const unsigned char *sk);


#endif

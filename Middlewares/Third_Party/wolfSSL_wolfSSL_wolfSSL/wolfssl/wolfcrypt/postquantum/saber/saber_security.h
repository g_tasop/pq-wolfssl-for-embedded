/*
 * saber_security.h
 *
 *  Created on: Jan 5, 2021
 *      Author: gtasop
 */

#ifndef SABER_SECURITY_H_
#define SABER_SECURITY_H_

/* 						Chose security level							*/
/*----------------------------------------------------------------------*/
	#define SABER_SECURITY_LEVEL 1		//HAVE_LIGHTSABER		//		|
//	#define SABER_SECURITY_LEVEL 3		//HAVE_VANILLASABER		//		|
//	#define SABER_SECURITY_LEVEL 5		//HAVE_FIRESABER		//		|
/*----------------------------------------------------------------------*/

/* 						Chose optimization method						*/
/*----------------------------------------------------------------------*/
//	#define SPEED_OPT					// Speed optimization	//		|
	#define STACK_OPT					// Stack optimization	//		|
/*----------------------------------------------------------------------*/


#endif /* SABER_SECURITY_H_ */

#ifndef PARAMS_KYBER_H
#define PARAMS_KYBER_H

#include <wolfcrypt/postquantum/kyber/kyber_security.h>

/*					    SECURITY LEVEL				   */
/* ----------------------------------------------------*/
#if KYBER_SECURITY_LEVEL == 1				//			|
	#define KYBER_K 2						//			|
#elif KYBER_SECURITY_LEVEL == 3				//			|
	#define KYBER_K 3						//			|
#else /* KYBER_SECURITY_LEVEL == 5 */		//			|
	#define KYBER_K 4						//			|
#endif										//			|
/* ----------------------------------------------------*/

///* OVERRIDE */
//#undef KYBER_K
//#define KYBER_K 2
///* OVERRIDE */

/* Don't change parameters below this line */

#define KYBER_N 256
#define KYBER_Q 3329

#if KYBER_K == 2
	#define KYBER_ETA1 3
	#define KYBER_ETA2 2
#elif KYBER_K ==3 || KYBER_K ==4
	#define KYBER_ETA 2
#else
#error "Unsupported KYBER parameter."
#endif

#define KYBER_SYMBYTES 32   /* size in bytes of hashes, and seeds */
#define KYBER_SSBYTES  32   /* size in bytes of shared key */

#define KYBER_POLYBYTES              384
#define KYBER_POLYVECBYTES           (KYBER_K * KYBER_POLYBYTES)

#if KYBER_K == 2 || KYBER_K ==3
	#define KYBER_POLYCOMPRESSEDBYTES    128
	#define KYBER_POLYVECCOMPRESSEDBYTES (KYBER_K * 320)
#elif KYBER_K ==4
	#define KYBER_POLYCOMPRESSEDBYTES    160
	#define KYBER_POLYVECCOMPRESSEDBYTES (KYBER_K * 352)
#else
#error "Unsupported KYBER parameter."
#endif


#define KYBER_INDCPA_MSGBYTES       KYBER_SYMBYTES
#define KYBER_INDCPA_PUBLICKEYBYTES (KYBER_POLYVECBYTES + KYBER_SYMBYTES)
#define KYBER_INDCPA_SECRETKEYBYTES (KYBER_POLYVECBYTES)
#define KYBER_INDCPA_BYTES          (KYBER_POLYVECCOMPRESSEDBYTES + KYBER_POLYCOMPRESSEDBYTES)

#define KYBER_PUBLICKEYBYTES  (KYBER_INDCPA_PUBLICKEYBYTES)
#define KYBER_SECRETKEYBYTES  (KYBER_INDCPA_SECRETKEYBYTES +  KYBER_INDCPA_PUBLICKEYBYTES + 2*KYBER_SYMBYTES) /* 32 bytes of additional space to save H(pk) */
#define KYBER_CIPHERTEXTBYTES  KYBER_INDCPA_BYTES

#endif /* PARAMS_KYBER_H */

/*
 * security.h
 *
 *  Created on: Dec 17, 2020
 *      Author: gtasop
 */

#ifndef SECURITY_H_
#define SECURITY_H_

/* ------------------------------------------------*/
//#define SECURITY_LEVEL 2 	//	choose				|
//#define SECURITY_LEVEL 3	//	security			|
#define SECURITY_LEVEL 4	//	level				|
/*-------------------------------------------------*/
/* ------------------------------------------------*/
#define SIGN_STACK			//	  sign				|
//#define SIGN_SPEED		//	 strategy			|
/* ------------------------------------------------*/

#endif /* SECURITY_H_ */

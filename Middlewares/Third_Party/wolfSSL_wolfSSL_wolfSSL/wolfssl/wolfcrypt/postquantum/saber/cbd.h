#ifndef CBD_SABER_H
#define CBD_SABER_H

#include "SABER_params.h"
#include <stdint.h>

void saber_cbd(uint16_t s[SABER_N], const uint8_t buf[SABER_POLYCOINBYTES]);

#endif

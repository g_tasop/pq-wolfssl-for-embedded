#ifndef CBD_H
#define CBD_H

#include <wolfcrypt/postquantum/kyber/poly.h>

/*--------------------------------------------------------------KYBER 512 ---------------------------------------------------------------- */
#if KYBER_K == 2
	void cbd_eta1(poly *r, const unsigned char *buf, int add);
	void cbd_eta2(poly *r, const unsigned char *buf, int add);
/*------------------------------------------------------KYBER 768 or KYBER 1024 ---------------------------------------------------------- */

#elif KYBER_K ==3 || KYBER_K ==4
	void cbd(poly *r, const unsigned char *buf, int add);
#else
#error "Unsupported KYBER parameter."
#endif

#endif

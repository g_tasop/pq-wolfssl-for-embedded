/*
 * frodo_security.h
 *
 *  Created on: Jul 21, 2022
 *      Author: gtasop
 */

#ifndef THIRD_PARTY_WOLFSSL_WOLFSSL_WOLFSSL_WOLFSSL_WOLFCRYPT_POSTQUANTUM_FRODO_FRODO_SECURITY_H_
#define THIRD_PARTY_WOLFSSL_WOLFSSL_WOLFSSL_WOLFSSL_WOLFCRYPT_POSTQUANTUM_FRODO_FRODO_SECURITY_H_

/*---------------------------------------------*/
	#define FRODO_SECURITY_LEVEL 1		//		|
/*---------------------------------------------*/
	#define FRODO_SHAKE					//		|
//	#define FRODO_AES					//		|
/*---------------------------------------------*/

#endif /* THIRD_PARTY_WOLFSSL_WOLFSSL_WOLFSSL_WOLFSSL_WOLFCRYPT_POSTQUANTUM_FRODO_FRODO_SECURITY_H_ */

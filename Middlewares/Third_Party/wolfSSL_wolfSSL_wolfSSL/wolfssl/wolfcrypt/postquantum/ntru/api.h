#ifndef NTRU_PQM4_API_H
#define NTRU_PQM4_API_H

#include <stdint.h>

#undef CRYPTO_PUBLICKEYBYTES
#undef CRYPTO_SECRETKEYBYTES
#undef CRYPTO_CIPHERTEXTBYTES
#undef CRYPTO_BYTES
#undef CRYPTO_ALGNAME

#define CRYPTO_SECRETKEYBYTES 935
#define CRYPTO_PUBLICKEYBYTES 699
#define CRYPTO_CIPHERTEXTBYTES 699
#define CRYPTO_BYTES 32

#define CRYPTO_ALGNAME "NTRU-HPS2048509"

int ntru_crypto_kem_keypair(uint8_t *pk, uint8_t *sk);

int ntru_crypto_kem_enc(uint8_t *c, uint8_t *k, const uint8_t *pk);

int ntru_crypto_kem_dec(uint8_t *k, const uint8_t *c, const uint8_t *sk);

#endif

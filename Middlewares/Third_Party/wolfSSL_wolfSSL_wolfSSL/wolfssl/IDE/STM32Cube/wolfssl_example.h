/* wolfssl_example.h
 *
 * Copyright (C) 2006-2020 wolfSSL Inc.
 *
 * This file is part of wolfSSL.
 *
 * wolfSSL is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * wolfSSL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335, USA
 */

/*
 * Modified by: George Tasopoulos, Industrial Systems Institute, RC ATHENA, 2023
 * This file has been modified, to enable post-quantum algorithm integration
 * into wolfSSL.
 */

#ifndef WOLFSSL_EXAMPLE_H_
#define WOLFSSL_EXAMPLE_H_

#ifdef HAVE_CONFIG_H
    #include <config.h>
#endif

#ifndef WOLFSSL_USER_SETTINGS
	#include <wolfssl/options.h>
#endif
#include <wolfssl/wolfcrypt/settings.h>
#include <wolfssl/ssl.h>
#include <wolfssl/wolfcrypt/error-crypt.h>
#include <wolfcrypt/test/test.h>
#include <wolfcrypt/benchmark/benchmark.h>

#ifndef SINGLE_THREADED
#include <cmsis_os.h>
#endif

#ifndef WOLF_EXAMPLES_STACK

#define WOLFSSL_DEBUG_MEMORY // for PrintMemStats

// Include to switch between the two variant of FrodoKEM (AES and SHAKE)
#include <wolfcrypt/postquantum/frodo/frodo_security.h>

/*
 * George Tasopoulos, ISI RC, Feb. 2023
 *
 *
 * Stack memory configuration of the threads for PQ and traditional TLS
 * AND for PQ and traditional benchmarks. The rest of the memory is considered
 * heap memory for dynamic allocations as in:
 *
 * 		configTOTAL_HEAP_SIZE - WOLF_STACK = Available heap memory
 *
 * where WOLF_STACK = WOLF_DEMO_STACK either WOLF_CLIENT_STACK either WOLF_SERVER_STACK
 * depending on the use case.
 *
 * Following is the carefully adjusted stack memory assignment of each thread, to avoid memory errors.
 * It is changing the memory for each thread automatically, with the corresponding defines, without the
 * need for any user adjustment.
 */

// ----------------------------------------------------- Post-quantum TLS ---------------------------------------------------//

	#ifdef HAVE_POSTQUANTUM
		#ifdef HAVE_DILITHIUM					// TLS with Dilithium
			#if defined(HAVE_FRODO) && defined(FRODO_AES)
				#define WOLF_DEMO_STACK  	1*1024 - 500			// The stack memory assigned to thread that runs "void wolfCryptDemo(void* argument)"
				#define WOLF_CLIENT_STACK  	83500					// The stack memory assigned to thread that runs "static void server_thread(void* args)"
				#define WOLF_SERVER_STACK  	73*1024					// The stack memory assigned to thread that runs "static void client_thread(void* args)"
			#elif defined(HAVE_FRODO) && defined(FRODO_SHAKE)
				#define WOLF_DEMO_STACK  	2*1024 - 500			// The stack memory assigned to thread that runs "void wolfCryptDemo(void* argument)"
				#define WOLF_CLIENT_STACK  	80000//73500					// The stack memory assigned to thread that runs "static void server_thread(void* args)"
				#define WOLF_SERVER_STACK  	73*1024					// The stack memory assigned to thread that runs "static void client_thread(void* args)"
			#elif defined(HAVE_BIKE)
				#define WOLF_DEMO_STACK  	2*1024 - 500
				#define WOLF_CLIENT_STACK  	91*1024
				#define WOLF_SERVER_STACK  	92*1024
			#elif defined(HAVE_HQC)
				#define WOLF_DEMO_STACK  	3*1024
				#define WOLF_CLIENT_STACK  	83*1024
				#define WOLF_SERVER_STACK  	83*1024
			#elif defined(HAVE_SIKE)
				#define WOLF_DEMO_STACK  	3*1024
				#define WOLF_CLIENT_STACK  	83*1024
				#define WOLF_SERVER_STACK  	83*1024
			#elif defined(HAVE_KYBER)
				#define WOLF_DEMO_STACK  	3*1024
				#define WOLF_CLIENT_STACK  	69204						// Minimum client stack before Balloc errors (68 kB) or (69204 bytes)
				#define WOLF_SERVER_STACK  	68*1024						// Maximum client stack before Out of memory errors (Heap memory) (92756 bytes)
			#elif defined(HAVE_NTRULPR)
				#define WOLF_DEMO_STACK  	10*1024
				#define WOLF_CLIENT_STACK  	70*1024
				#define WOLF_SERVER_STACK  	70*1024
			#elif defined(HAVE_SABER)
				#define WOLF_DEMO_STACK  	3*1024
				#define WOLF_CLIENT_STACK  	69204
				#define WOLF_SERVER_STACK  	68*1024
			#else
				#define WOLF_DEMO_STACK  	3*1024
				#define WOLF_CLIENT_STACK  	77*1024
				#define WOLF_SERVER_STACK  	77*1024
			#endif
		#endif/* HAVE_DILITHIUM */

		#ifdef HAVE_FALCON						// TLS with Falcon
			#define WOLF_DEMO_STACK  	7*1024
			#define WOLF_CLIENT_STACK  	6*1024
			#define WOLF_SERVER_STACK  	6*1024
		#endif /* HAVE_FALCON */

		#ifdef HAVE_SPHINCS						// TLS with Sphincs
			#define WOLF_DEMO_STACK  	3*1024
			#define WOLF_CLIENT_STACK  	67*1024
			#define WOLF_SERVER_STACK  	67*1024
		#endif /* HAVE_SPHINCS */

		#ifdef HAVE_PICNIC						// TLS with Picnic
			#define WOLF_DEMO_STACK  	1500
			#define WOLF_CLIENT_STACK  	88500
			#define WOLF_SERVER_STACK  	87*1024
		#endif /* HAVE_PICNIC */



	#elif !defined(BENCH_POSTQUANTUM) \
	&& defined(HAVE_TRADITIONAL)				// TLS with traditional algorithms
		#define WOLF_DEMO_STACK  	15*1024
		#define WOLF_CLIENT_STACK  	13*1024
		#define WOLF_SERVER_STACK  	13*1024
	#endif /* HAVE_POSTQUANTUM */

// ----------------------------------------------------- Primitives Benchmark ---------------------------------------------------//

	#ifdef BENCH_POSTQUANTUM 		// Post-quantum primitives
		#ifdef BENCH_FALCON											// Bench just Falcon
			#define WOLF_DEMO_STACK  	10*1024
			#define WOLF_CLIENT_STACK  	83*1024
			#define WOLF_SERVER_STACK  	83*1024
		#elif defined(BENCH_BIKE)									// Bench just Bike
			#define WOLF_DEMO_STACK  	115*1024
			#define WOLF_CLIENT_STACK  	83*1024
			#define WOLF_SERVER_STACK  	83*1024
		#elif defined (BENCH_FRODO)
			#define WOLF_DEMO_STACK  	125*1024
			#define WOLF_CLIENT_STACK  	83*1024
			#define WOLF_SERVER_STACK  	83*1024
		#else														// Bench the rest of the PQ algorithms
			#define WOLF_DEMO_STACK  	90*1024						// Maybe 120*1024 ??
			#define WOLF_CLIENT_STACK  	83*1024
			#define WOLF_SERVER_STACK  	83*1024
		#endif /* BENCH_FALCON */
	#endif

	#if !defined(HAVE_POSTQUANTUM) && \
	!defined(BENCH_POSTQUANTUM) && \
	!defined(HAVE_TRADITIONAL) 			// Traditional primitives
			#define WOLF_DEMO_STACK  	120*1024
			#define WOLF_CLIENT_STACK  	83*1024
			#define WOLF_SERVER_STACK  	83*1024
	#endif /* BENCH_POSTQUANTUM */

#endif /* WOLF_EXAMPLES_STACK */

#ifdef CMSIS_OS2_H_
void wolfCryptDemo(void* argument);
#else
void wolfCryptDemo(void const * argument);
#endif

#endif /* WOLFSSL_EXAMPLE_H_ */

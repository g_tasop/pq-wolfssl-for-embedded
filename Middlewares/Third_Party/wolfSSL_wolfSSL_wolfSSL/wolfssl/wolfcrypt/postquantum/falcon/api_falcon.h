#ifndef API_FALCON_H
#define API_FALCON_H

#include <stddef.h>
#include <wolfcrypt/postquantum/falcon/falcon_security.h>

#if FALCON_SECURITY_LEVEL == 1
	#define HAVE_FALCON512
	#define CRYPTO_SECRETKEYBYTES   1281
	#define CRYPTO_PUBLICKEYBYTES   897
	#define CRYPTO_BYTES            690
	#define CRYPTO_ALGNAME          "Falcon-512"
#else /* FALCON_SECURITY_LEVEL == 5 */
	#define HAVE_FALCON1024
	#define CRYPTO_SECRETKEYBYTES   2305
	#define CRYPTO_PUBLICKEYBYTES   1793
	#define CRYPTO_BYTES            1330
	#define CRYPTO_ALGNAME          "Falcon-1024"
#endif /* FALCON_SECURITY_LEVEL */

int falcon_crypto_sign_keypair(unsigned char *pk, unsigned char *sk);

int falcon_crypto_sign(unsigned char *sm, size_t *smlen,
	const unsigned char *m, size_t mlen,
	const unsigned char *sk);

int falcon_crypto_sign_open(unsigned char *m, size_t *mlen,
	const unsigned char *sm, size_t smlen,
	const unsigned char *pk);

/* ------------------------- ADDED EXTRA FUNCTIONS ----------------------- */

int falcon_crypto_sign_signature(
	unsigned char *sig, size_t *siglen,
    const unsigned char *m, size_t mlen, const unsigned char *sk);

int falcon_crypto_sign_verify(
    const unsigned char *sig, size_t siglen,
    unsigned char *m, size_t mlen, const unsigned char *pk);

/* ----------------------------------------------------------------------- */
#endif

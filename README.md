# WolfSSL 4.7.0 with Postquantum Algorithms for Nucleo Boards (F439ZI and F429ZI)

## Description

This is a tweaked version of WolfSSL that enables the use of Postquantum algorithms for key exchange and authentication in TLS 1.3, designed to run on Nucleo Boards F439ZI and F429ZI. The version we are using is [WolfSSL 4.7.0](https://github.com/wolfSSL/wolfssl/releases/tag/v4.7.0-stable) and the post-quantum algorithms implementations are from [pqm4](https://github.com/mupq/pqm4), optimized for the ARM Cortex-M4 processor and some implementations from [PQClean](https://github.com/PQClean/PQClean). The post-quantum algorithms that are supported are:

### Key Encapsualtion Mechanisms (KEMs)

- [x] CRYSTALS-Kyber

- [x] Saber

- [x] NTRU

- [x] SIKE

- [x] BIKE

- [x] HQC

- [x] NTRU Lprime

- [x] FrodoKEM

### Digital Signature Algorithms

- [x] CRYSTALS-Dilithium

- [x] Falcon

- [x] SPHINCS+

- [x] Picnic3



For networking, the [LwIP](https://savannah.nongnu.org/projects/lwip/) TCP/IP stack (version 2.1.2) is used and the [FreeRTOS](https://www.freertos.org/), as a real time operating system.

## Target Boards

This version is developed and is mainly targeting the [STM32 Nucleo F439ZI](https://www.st.com/en/evaluation-tools/nucleo-f439zi.html) board but it is also able to run in other boards of this family (e.g STM32 Nucleo F429ZI) with minor configuration.


## Installation


### STM32 Cube IDE

1. Download this repository as a ziped file:

2. Using STM32 Cube IDE, go to `File > Import > Projects from Folder or Archive`, select the `Archive...` button and select the file `pq-wolfssl-for-embedded-main.zip`.

3. Click `Finish` and the project is ready to be built.

## Quickstart

**IMPORTANT: If you want to take measurements make sure to build with the RELEASE configuration for maximum optimization**

The `Core/src/main.c` file, initializes all the peripherals and starts the FREERTOS kernel.

It then creates a new thread, with the regarding function (in our case the function `wolfCryptDemo`).

`wolfCryptDemo` prints the following menu, with the different functionalities that it is providing.

```
				MENU

	t. WolfCrypt Test
	b. WolfCrypt Benchmark
	e. Show Cipher List
	c. PQ-TLS13 Client
	s. PQ-TLS13 Server
	r. Restart Thread

```

The options that we have tweaked are `WolfCrypt Benchmark`,  `PQ-TLS13 Client`, `PQ-TLS13 Server` and `Restart Thread` and are described below:

- `WolfCrypt Benchmark`: This function benchmarks all the enabled PQ primitives (e.g Key generation, Sign, Encapsualate etc.).

- `PQ-TLS13 Client`: This function creates another thread that runs the `bench_tls_client` function. It will try to connect to a remote PQ TLS13 server using the hard-coded IP address and Port and run the TLS benchmark code.

- `PQ-TLS13 Server`: Similar to the `PQ-TLS13 Client`, it creates a thread the runs the `bench_tls_server` function and will wait for a client to connect on the IP address that the acces point has assigned to the board and on the hard-coded Port.

- `Restart Thread`: This option restarts the `wolfCryptDemo` thread. It mitigates some implementation bug of this version that leads to some memory leaks, thus not enabling us to run another benchmark after one is done.

Below is the *Configuration* section that has all the information needed to control WolfSSL and the PQ algorithms that are have provided.

## Configuring

To configure this project you must edit the configuration file: `wolfssl/wolfSSL.I-CUBE-wolfSSL_conf.h`.

### 0. Configuring for board

The uploaded code is meant to be used with NUCLEO-F439ZI, the board with the hardware accelerated crypto. For the code to be used with NUCLEO-F429ZI, we have to comment out the 2 defines: `NO_STM32_HASH` and `NO_STM32_CRYPTO`, in line 198-199 of the configuration file. This block of code should look like this, if it is meant to be used **WITHOUT** hardware acceleration:

```

/* Hardware Crypto - uncomment as available on hardware */
//  #undef  NO_STM32_HASH
//  #undef  NO_STM32_CRYPTO
    #define STM32_HAL_V2
    #define STM32_RNG
    #define STM32_AESGCM_PARTIAL /* allow partial blocks and add auth info (header) */

```

### 1. Enable PQ algorithms and choose security level

in line 227-332 of `wolfssl/wolfSSL.I-CUBE-wolfSSL_conf.h` we can configure the "Post-quantum Primitives Benchmark" and the "Post-quantum TLS 1.3 Benchmark" and some other functionalities that are included. We can choose which algorithms will be enabled and benchmarked in either of the two benchmarks.

You can enable any algorithm in the primitive benchmark BUT only one of the authentication methods and only one of the key exchange methods in the wolfSSL TLS benchmark.

**   **From now on we define *"Middlewares/Third_Party/wolfSSL_wolfSSL_wolfSSL/wolfssl"* as "/"** 	**

### 2. Choose the security strength of PQ algorithms


edit the following files: 

*"/wolfcrypt/postquantum/kyber/kyber_security.h"*

*"/wolfcrypt/postquantum/dilithium/dilithium_security.h"*

*"/wolfcrypt/postquantum/saber/saber_security.h"*

*"/wolfcrypt/postquantum/falcon/falcon_security.h"*

and so on...

namely we have the following equivelences:

**SECURITY_LEVEL 1** --> Kyber512, LightSaber, Dilithium2, Falcon512

**SECURITY_LEVEL 3** --> Kyber768, Saber, Dilithium3

**SECURITY_LEVEL 5** --> Kyber1024, FireSaber, Falcon1024

and so on...


### Troubleshooting

1) In newer IDE versions that use different toolchains there are some errors regarding the inclusion of 2 header files (e.g ../../settings.h). These header files are not being used by our implementation and can be **commented out** without any problem. This should make the whole project compile without any further errors.

2) If the benchmark shows an error like this:

```
ERROR: failed to connect
Client Error: -1 (unknown error number)
```

try re-running the benchmark OR try debugging it with an IDE.


## Usage

To use this project to establish a PQ TLS 1.3 connection to a remote PC (i.e a PC running [this](https://gitlab.com/g_tasop/wolfssl-pq-for-pc) project) you should do:

1. Set up a server to run on the board by "downloading" the code to the board.

2. Get the internal IP if it's a local network or the external IP if it's connected to the Internet and run the client from the PC targeting that IP.

OR

1. Set up a server to run on the remote PC and get its IP. For example, the IP of the remote machine might be "192.168.1.14" on port 11112.

2. Hard-code the IP address and Port by editing the `IDE/STM32Cube/wolfssl_example.c` file at line ~59.

3. Get the client to run by "downloading" the code to the board.

## Tested

This project's build and usage has been tested successfully on the following platforms:

- [x] STM32 Nucleo F429ZI
- [x] STM32 Nucleo F439ZI

## Extra info

If you want to test the performance on **traditional** TLS 1.3 you only need to:

1. Comment out the Post-quantum defines that we added in file `wolfssl/wolfSSL.I-CUBE-wolfSSL_conf.h`.

2. Uncomment `#define HAVE_TRADITIONAL`.

3. Compile and run. 

The default algorithms that WolfSSL 4.7.0 uses are: 

- ECDHE (Elliptic Curve Diffie-Hellman Ephemeral) with curve **SECP256R1** for key exchange.

- RSA-2048 for digital signatures.

To override them and use ECC in authentication you have to:

1.  Uncomment `#define MY_HAVE_ECC`.

2. Compile and run.

Now the traditional algorithms that are used are:

- ECDHE (Elliptic Curve Diffie-Hellman Ephemeral) with curve **SECP256R1** for key exchange.

- ECDSA (Elliptic Curve DSA) with curve **SECP256R1** for authentication.



## License
For all the licenses of the components of this project, see file `Third-party-licenses.txt`.

(the original README follows)

---

---


<a href="https://repology.org/project/wolfssl/versions">
    <img src="https://repology.org/badge/vertical-allrepos/wolfssl.svg" alt="Packaging status" align="right">
</a>

# wolfSSL Embedded SSL/TLS Library

The [wolfSSL embedded SSL library](https://www.wolfssl.com/products/wolfssl/) 
(formerly CyaSSL) is a lightweight SSL/TLS library written in ANSI C and
targeted for embedded, RTOS, and resource-constrained environments - primarily
because of its small size, speed, and feature set.  It is commonly used in
standard operating environments as well because of its royalty-free pricing
and excellent cross platform support. wolfSSL supports industry standards up
to the current [TLS 1.3](https://www.wolfssl.com/tls13) and DTLS 1.2, is up to
20 times smaller than OpenSSL, and offers progressive ciphers such as ChaCha20,
Curve25519, NTRU, and Blake2b. User benchmarking and feedback reports
dramatically better performance when using wolfSSL over OpenSSL.

wolfSSL is powered by the wolfCrypt cryptography library. Two versions of
wolfCrypt have been FIPS 140-2 validated (Certificate #2425 and
certificate #3389). FIPS 140-3 validation is in progress. For additional
information, visit the [wolfCrypt FIPS FAQ](https://www.wolfssl.com/license/fips/)
or contact fips@wolfssl.com.

## Why Choose wolfSSL?

There are many reasons to choose wolfSSL as your embedded, desktop, mobile, or
enterprise SSL/TLS solution. Some of the top reasons include size (typical
footprint sizes range from 20-100 kB), support for the newest standards
(SSL 3.0, TLS 1.0, TLS 1.1, TLS 1.2, TLS 1.3, DTLS 1.0, and DTLS 1.2), current
and progressive cipher support (including stream ciphers), multi-platform,
royalty free, and an OpenSSL compatibility API to ease porting into existing
applications which have previously used the OpenSSL package. For a complete
feature list, see [Chapter 4](https://www.wolfssl.com/docs/wolfssl-manual/ch4/)
of the wolfSSL manual.

## Notes, Please Read

**Note 1)**
wolfSSL as of 3.6.6 no longer enables SSLv3 by default.  wolfSSL also no longer
supports static key cipher suites with PSK, RSA, or ECDH. This means if you
plan to use TLS cipher suites you must enable DH (DH is on by default), or
enable ECC (ECC is on by default), or you must enable static key cipher suites
with one or more of the following defines:

    WOLFSSL_STATIC_DH
    WOLFSSL_STATIC_RSA
    WOLFSSL_STATIC_PSK

Though static key cipher suites are deprecated and will be removed from future
versions of TLS.  They also lower your security by removing PFS.  Since current
NTRU suites available do not use ephemeral keys, ```WOLFSSL_STATIC_RSA``` needs
to be used in order to build with NTRU suites.

When compiling ssl.c, wolfSSL will now issue a compiler error if no cipher
suites are available. You can remove this error by defining
```WOLFSSL_ALLOW_NO_SUITES``` in the event that you desire that, i.e., you're
not using TLS cipher suites.

**Note 2)**
wolfSSL takes a different approach to certificate verification than OpenSSL
does. The default policy for the client is to verify the server, this means
that if you don't load CAs to verify the server you'll get a connect error,
no signer error to confirm failure (-188).

If you want to mimic OpenSSL behavior of having SSL\_connect succeed even if
verifying the server fails and reducing security you can do this by calling:

    wolfSSL_CTX_set_verify(ctx, SSL_VERIFY_NONE, 0);

before calling wolfSSL\_new();. Though it's not recommended.

**Note 3)**
The enum values SHA, SHA256, SHA384, SHA512 are no longer available when
wolfSSL is built with --enable-opensslextra (```OPENSSL_EXTRA```) or with the
macro ```NO_OLD_SHA_NAMES```. These names get mapped to the OpenSSL API for a
single call hash function. Instead the name WC_SHA, WC_SHA256, WC_SHA384 and
WC_SHA512 should be used for the enum name.

# wolfSSL Release 4.7.0 (February 16, 2021)
Release 4.7.0 of wolfSSL embedded TLS has bug fixes and new features including:

### New Feature Additions
* Compatibility Layer expansion SSL_get_verify_mode, X509_VERIFY_PARAM API, X509_STORE_CTX API added
* WOLFSSL_PSK_IDENTITY_ALERT macro added for enabling a subset of TLS alerts
* Function wolfSSL_CTX_NoTicketTLSv12 added to enable turning off session tickets with TLS 1.2 while keeping TLS 1.3 session tickets available
* Implement RFC 5705: Keying Material Exporters for TLS
* Added --enable-reproducible-build flag for making more deterministic library outputs to assist debugging

### Fixes
* Fix to free mutex when cert manager is free’d
* Compatibility layer EVP function to return the correct block size and type
* DTLS secure renegotiation fixes including resetting timeout and retransmit on duplicate HelloRequest
* Fix for edge case with shrink buffer and secure renegotiation
* Compile fix for type used with curve448 and PPC64
* Fixes for SP math all with PPC64 and other embedded compilers
* SP math all fix when performing montgomery reduction on one word modulus
* Fixes to SP math all to better support digit size of 8-bit
* Fix for results of edge case with SP integer square operation
* Stop non-ct mod inv from using register x29 with SP ARM64 build
* Fix edge case when generating z value of ECC with SP code
* Fixes for PKCS7 with crypto callback (devId) with RSA and RNG
* Fix for compiling builds with RSA verify and public only
* Fix for PKCS11 not properly exporting the public key due to a missing key type field
* Call certificate callback with certificate depth issues
* Fix for out-of-bounds read in TLSX_CSR_Parse()
* Fix incorrect AES-GCM tag generation in the EVP layer
* Fix for out of bounds write with SP math all enabled and an edge case of calling sp_tohex on the result of sp_mont_norm
* Fix for parameter check in sp_rand_prime to handle 0 length values
* Fix for edge case of failing malloc resulting in an out of bounds write with SHA256/SHA512 when small stack is enabled


### Improvements/Optimizations
* Added --enable-wolftpm option for easily building wolfSSL to be used with wolfTPM
* DTLS macro WOLFSSL_DTLS_RESEND_ONLY_TIMEOUT added for resending flight only after a timeout
* Update linux kernel module to use kvmalloc and kvfree
* Add user settings option to cmake build
* Added support for AES GCM session ticket encryption
* Thread protection for global RNG used by wolfSSL_RAND_bytes function calls
* Sanity check on FIPs configure flag used against the version of FIPs bundle
* --enable-aesgcm=table now is compatible with --enable-linuxkm
* Increase output buffer size that wolfSSL_RAND_bytes can handle
* Out of directory builds resolved, wolfSSL can now be built in a separate directory than the root wolfssl directory

### Vulnerabilities
* [HIGH] CVE-2021-3336: In earlier versions of wolfSSL there exists a potential man in the middle attack on TLS 1.3 clients. Malicious attackers with a privileged network position can impersonate TLS 1.3 servers and bypass authentication. Users that have applications with client side code and have TLS 1.3 turned on, should update to the latest version of wolfSSL. Users that do not have TLS 1.3 turned on, or that are server side only, are NOT affected by this report. For the code change see https://github.com/wolfSSL/wolfssl/pull/3676.
* [LOW] In the case of using custom ECC curves there is the potential for a crafted compressed ECC key that has a custom prime value to cause a hang when imported. This only affects applications that are loading in ECC keys with wolfSSL builds that have compressed ECC keys and custom ECC curves enabled.
* [LOW] With TLS 1.3 authenticated-only ciphers a section of the server hello could contain 16 bytes of uninitialized data when sent to the connected peer. This affects only a specific build of wolfSSL with TLS 1.3 early data enabled and using authenticated-only ciphers with TLS 1.3.


For additional vulnerability information visit the vulnerability page at
https://www.wolfssl.com/docs/security-vulnerabilities/

See INSTALL file for build instructions.
More info can be found on-line at https://wolfssl.com/wolfSSL/Docs.html



# Resources

[wolfSSL Website](https://www.wolfssl.com/)

[wolfSSL Wiki](https://github.com/wolfSSL/wolfssl/wiki)

[FIPS 140-2/140-3 FAQ](https://wolfssl.com/license/fips)

[wolfSSL Documentation](https://wolfssl.com/wolfSSL/Docs.html)

[wolfSSL Manual](https://wolfssl.com/wolfSSL/Docs-wolfssl-manual-toc.html)

[wolfSSL API Reference](https://wolfssl.com/wolfSSL/Docs-wolfssl-manual-17-wolfssl-api-reference.html)

[wolfCrypt API Reference](https://wolfssl.com/wolfSSL/Docs-wolfssl-manual-18-wolfcrypt-api-reference.html)

[TLS 1.3](https://www.wolfssl.com/docs/tls13/)

[wolfSSL Vulnerabilities](https://www.wolfssl.com/docs/security-vulnerabilities/)



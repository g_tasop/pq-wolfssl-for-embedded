/*
 *  Generates a random block using wolfcrypt's implementation
 *  with the underlying RNG of the STMF4
 *
 *   author: George Tasopoulos
 */
#include <wolfcrypt/postquantum/common/randombytes.h>
#include "wolfssl/wolfcrypt/random.h"

int randombytes(uint8_t *buf, size_t n) {
    WC_RNG myrng;

    wc_InitRng(&myrng);
    wc_RNG_GenerateBlock(&myrng, buf, n);
    wc_FreeRng(&myrng);

    return 0;
}
